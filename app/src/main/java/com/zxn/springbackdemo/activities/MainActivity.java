package com.zxn.springbackdemo.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zxn.springback.SpringbackLayout;
import com.zxn.springback.models.User;
import com.zxn.springbackdemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    // region Constants
    private static final int STACK_SIZE = 1;//4
    @BindView(R.id.fl_container)
    SpringbackLayout flContainer;
    // endregion

    // region Views
//    private TinderStackLayout tinderStackLayout;
    // endregion

    // region Member Variables
    private String[] displayNames, userNames, avatarUrls;
    private int index = 0;
    // endregion

    // region Listeners
    // endregion
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        onInitView();

//        displayNames = getResources().getStringArray(R.array.display_names);
//        userNames = getResources().getStringArray(R.array.usernames);
//        avatarUrls = getResources().getStringArray(R.array.avatar_urls);
//        tinderStackLayout = (TinderStackLayout) findViewById(R.id.tsl);
//        TinderCardView tc;
//        for(int i=index; index<i+STACK_SIZE; index++){
//            tc = new TinderCardView(this);
//            tc.bind(getUser(index));
//            tinderStackLayout.addCard(tc);
//        }

        /*tinderStackLayout.getPublishSubject()
                .observeOn(AndroidSchedulers.mainThread()) // UI Thread
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        if(integer == 1){
                            TinderCardView tc;
                            for(int i=index; index<i+(STACK_SIZE-1); index++){
                                tc = new TinderCardView(MainActivity.this);
                                tc.bind(getUser(index));
                                tinderStackLayout.addCard(tc);
                            }
                        }
                    }
                });*/

    }

    private void onInitView() {
        flContainer.springbackListener(new SpringbackLayout.OnSpringbackListener() {
            @Override
            public void onSpringback(SpringbackLayout view, int scrollState) {

                String text = "状态:" + (scrollState == SpringbackLayout.OnSpringbackListener.SCROLL_STATE_IDLE
                        ? "滑动距离太小"
                        : (scrollState == SpringbackLayout.OnSpringbackListener.SCROLL_STATE_LEFT ? "左边回弹" : "右边回弹"));
                Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // region Helper Methods
    private User getUser(int index) {
        User user = new User();
        user.setAvatarUrl(avatarUrls[index]);
        user.setDisplayName(displayNames[index]);
        user.setUsername(userNames[index]);
        return user;
    }
    // endregion
}

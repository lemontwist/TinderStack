package com.zxn.springback;

import android.animation.Animator;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zxn.springback.models.User;
import com.zxn.springback.utilities.DisplayUtility;


/**
 * Updated by zxn on 2020/3/31.
 */
public class SpringbackLayout extends FrameLayout implements View.OnTouchListener {

    // region Constants
    /**
     * 卡片旋转角度
     */
    private static final float CARD_ROTATION_DEGREES = 40.0f;
    /**
     * 徽章的旋转角度
     */
    private static final float BADGE_ROTATION_DEGREES = 15.0f;
    /**
     * 持续时间
     */
    private static final int DURATION = 300;
    // endregion

    // region Views
//    private ImageView imageView;
//    private TextView displayNameTextView;
//    private TextView usernameTextView;
//    private TextView likeTextView;
//    private TextView nopeTextView;
    // endregion

    // region Member Variables
    private float oldX;
    private float oldY;
    private float newX;
    private float newY;
    private float dX;
    private float dY;
    private float rightBoundary;
    private float leftBoundary;
    private int screenWidth;
    private int padding;
    private OnSpringbackListener mOnSpringbackListener;
    // endregion

    // region Constructors
    public SpringbackLayout(Context context) {
        super(context);
        //init(context, null);
    }

    public SpringbackLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SpringbackLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }
    // endregion

    // region View.OnTouchListener Methods
    @Override
    public boolean onTouch(final View view, MotionEvent motionEvent) {
        //TinderStackLayout tinderStackLayout = ((TinderStackLayout) view.getParent());
        //TinderCardView topCard = (TinderCardView) tinderStackLayout.getChildAt(tinderStackLayout.getChildCount() - 1);
        //if (topCard.equals(view)) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    oldX = motionEvent.getX();
                    oldY = motionEvent.getY();

                    // cancel any current animations
                    view.clearAnimation();
                    return true;
                case MotionEvent.ACTION_UP:
                    if (isCardBeyondLeftBoundary(view)) {
                        //RxBus.getInstance().send(new TopCardMovedEvent(-(screenWidth)));
                        //dismissCard(view, -(screenWidth * 2));
                        if (null != mOnSpringbackListener) {
                            mOnSpringbackListener.onSpringback(this, OnSpringbackListener.SCROLL_STATE_LEFT);
                        }
                    } else if(isCardBeyondRightBoundary(view)){
                        //RxBus.getInstance().send(new TopCardMovedEvent(screenWidth));
                        //dismissCard(view, (screenWidth * 2));
                        if (null != mOnSpringbackListener) {
                            mOnSpringbackListener.onSpringback(this,OnSpringbackListener.SCROLL_STATE_RIGHT);
                        }
                    } else {
                        if (null != mOnSpringbackListener) {
                            mOnSpringbackListener.onSpringback(this,OnSpringbackListener.SCROLL_STATE_IDLE);
                        }
                    }
                    //RxBus.getInstance().send(new TopCardMovedEvent(0));
                    resetCard(view);
                    return true;
                case MotionEvent.ACTION_MOVE:
                    newX = motionEvent.getX();
                    newY = motionEvent.getY();

                    dX = newX - oldX;
                    dY = newY - oldY;

                    float posX = view.getX() + dX;

                    //RxBus.getInstance().send(new TopCardMovedEvent(posX));

                    //控制不允许垂直向上或者向下滑动
                    if (Math.abs(dY) > Math.abs(dX)) {
                        return false;
                    }
                    // Set new position
                    view.setX(view.getX() + dX);
                    view.setY(view.getY() + dY);

                    setCardRotation(view, view.getX());

                    //updateAlphaOfBadges(posX);
                    return true;
                default:
                    return super.onTouchEvent(motionEvent);
            }
        }
        //return super.onTouchEvent(motionEvent);
    //}
    // endregion

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setOnTouchListener(null);
    }

    // region Helper Methods
    private void init(Context context, AttributeSet attrs) {
        if (!isInEditMode()) {
            //inflate(context, R.layout.tinder_card, this);

//            imageView = (ImageView) findViewById(R.id.iv);
//            displayNameTextView = (TextView) findViewById(R.id.display_name_tv);
//            usernameTextView = (TextView) findViewById(R.id.username_tv);
//            likeTextView = (TextView) findViewById(R.id.like_tv);
//            nopeTextView = (TextView) findViewById(R.id.nope_tv);
//
//            likeTextView.setRotation(-(BADGE_ROTATION_DEGREES));
//            nopeTextView.setRotation(BADGE_ROTATION_DEGREES);

            screenWidth = DisplayUtility.getScreenWidth(context);
            leftBoundary = screenWidth * (1.0f / 6.0f); // Left 1/6 of screen
            rightBoundary = screenWidth * (5.0f / 6.0f); // Right 1/6 of screen
            padding = DisplayUtility.dp2px(context, 16);

            setOnTouchListener(this);
        }
    }

    // Check if card's middle is beyond the left boundary
    private boolean isCardBeyondLeftBoundary(View view) {
        return (view.getX() + (view.getWidth() / 2) < leftBoundary);
    }

    // Check if card's middle is beyond the right boundary
    private boolean isCardBeyondRightBoundary(View view) {
        return (view.getX() + (view.getWidth() / 2) > rightBoundary);
    }

    private void dismissCard(final View view, int xPos) {
        view.animate()
                .x(xPos)
                .y(0)
                .setInterpolator(new AccelerateInterpolator())
                .setDuration(DURATION)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        /*ViewGroup viewGroup = (ViewGroup) view.getParent();
                        if(viewGroup != null) {
                            viewGroup.removeView(view);
                        }*/
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
    }

    /**
     * 卡片视图位置重置回归原位.
     *
     * @param view
     */
    private void resetCard(View view) {
        view.animate()
                .x(0)
                .y(0)
                .rotation(0)
                .setInterpolator(new OvershootInterpolator())
                .setDuration(DURATION);

//        likeTextView.setAlpha(0);
//        nopeTextView.setAlpha(0);
    }

    /**
     * 设置卡片的旋转角度
     *
     * @param view 旋转的view
     * @param posX x坐标
     */
    private void setCardRotation(View view, float posX) {
        float rotation = (CARD_ROTATION_DEGREES * (posX)) / screenWidth;
        int halfCardHeight = (view.getHeight() / 2);
        if (oldY < halfCardHeight - (2 * padding)) {
            view.setRotation(rotation);
        } else {
            view.setRotation(-rotation);
        }
    }

    /**
     * 设置徽章的透明度.
     * set alpha of like and nope badges
     *
     * @param posX
     */
    private void updateAlphaOfBadges(float posX) {
        float alpha = (posX - padding) / (screenWidth * 0.50f);
//        likeTextView.setAlpha(alpha);
//        nopeTextView.setAlpha(-alpha);
    }

    public void bind(User user) {
        if (user == null)
            return;

//        setUpImage(imageView, user);
//        setUpDisplayName(displayNameTextView, user);
//        setUpUsername(usernameTextView, user);
    }

    private void setUpImage(ImageView iv, User user) {
        String avatarUrl = user.getAvatarUrl();
        if (!TextUtils.isEmpty(avatarUrl)) {
            Picasso.with(iv.getContext())
                    .load(avatarUrl)
                    .into(iv);
        }
    }

    private void setUpDisplayName(TextView tv, User user) {
        String displayName = user.getDisplayName();
        if (!TextUtils.isEmpty(displayName)) {
            tv.setText(displayName);
        }
    }

    private void setUpUsername(TextView tv, User user) {
        String username = user.getUsername();
        if (!TextUtils.isEmpty(username)) {
            tv.setText(username);
        }
    }

    // endregion


    public interface OnSpringbackListener {

        /**
         * 回弹不生效,不算数
         */
        int SCROLL_STATE_IDLE = 0;

        /**
         * 从左边回弹回来
         */
        int SCROLL_STATE_LEFT = 1;

        /**
         * 从右边回弹回来
         */
        int SCROLL_STATE_RIGHT = 2;

        /**
         * 视图弹回监听.
         *
         * @param scrollState SCROLL_STATE_LEFT:左边弹回,SCROLL_STATE_RIGHT:右边弹回,SCROLL_STATE_IDLE:闲置状态.
         */
        void onSpringback(SpringbackLayout view, int scrollState);
    }

    public void springbackListener(OnSpringbackListener l) {
        this.mOnSpringbackListener = l;
    }
}
